'use strict';

var app = angular.module('iweb', ['ngRoute', 'ngResource']);

/*
 * Routes
 */

app.config(['$routeProvider', '$locationProvider',
		function($routeProvider, $locationProvider) {
			$routeProvider
				.when('/', {
					templateUrl: '/partials/index.html',
					controller: 'IndexController'
				})
			.when('/hosts', {
				templateUrl: '/partials/hosts.html',
				controller: 'HostsController'
			})
			.when('/hosts/create', {
				templateUrl: '/partials/create_host.html',
				controller: 'CreateHostController'
			})
			.when('/hosts/:hostId', {
				templateUrl: '/partials/host.html',
				controller: 'HostController'
			})
			.when('/hosts/:hostId/create', {
				templateUrl: '/partials/create_instance.html',
				controller: 'CreateInstanceController'
			})
			.otherwise({
				redirectTo: '/'
			});

			$locationProvider.html5Mode(true);
		}
]);

/*
 * Services
 */

app.factory('Host', function($resource) {
	return $resource('http://api.iwebhires.com/hosts/:hostId', {}, {
			query: {
				method: 'GET',
				params: {hostId: ''},
				isArray: true
			}
			});
});

app.factory('Instance', function($resource) {
	return $resource('http://api.iwebhires.com/hosts/:hostId/instances/:instanceId', {hostId: '@hostId', instanceId: ''});
});

/*
 * Directives
 */

var INTEGER_REGEXP = /^\-?\d+$/;
app.directive('integer', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      ctrl.$validators.integer = function(modelValue, viewValue) {
        if (ctrl.$isEmpty(modelValue)) {
          // consider empty models to be valid
          return true;
        }

        if (INTEGER_REGEXP.test(viewValue)) {
          // it is valid
          return true;
        }

        // it is invalid
        return false;
      };
    }
  };
});

/*
 * Controllers
 */

app.controller('IndexController', ['$scope', function($scope) {}]);

app.controller('HostsController', ['$scope', '$location', 'Host', function($scope, $location, Host) {
	$scope.hosts = Host.query();

	$scope.create = function() {
		$location.path('/hosts/create');
	};

	$scope.view = function(host) {
	  $location.path('/hosts/' + host.id);
	};

	$scope.delete = function(host) {
	  Host.delete({hostId: host.id}, function() {
	    $scope.hosts = Host.query();
	  });
	};
}]);

app.controller('CreateHostController', ['$scope', '$location', 'Host', function($scope, $location, Host) {
	$scope.host = {};

	$scope.save = function(host) {
	  if (!$scope.form.$valid) {
	    return;
	  }

	  var host = Host.save(host, function(host) {
	    $location.path('/hosts/' + host.id);
	  });
	};
}]);

app.controller('HostController', ['$scope', '$location', '$routeParams', 'Host', 'Instance', function($scope, $location, $routeParams, Host, Instance) {
	$scope.host = Host.get({hostId: $routeParams.hostId});
	$scope.instances = Instance.query({hostId: $routeParams.hostId});

	$scope.create = function() {
		$location.path('/hosts/' + $routeParams.hostId + '/create');
	};

	$scope.delete = function(instance) {
	  Instance.delete({hostId: $routeParams.hostId, instanceId: instance.id}, function() {
	    $scope.instances = Instance.query({hostId: $routeParams.hostId});
	  });
	};
}]);

app.controller('CreateInstanceController', ['$scope', '$location', '$routeParams', 'Instance', function($scope, $location, $routeParams, Instance) {
	$scope.instance = {};

	$scope.save = function(instance) {
	  if (!$scope.form.$valid) {
	    return;
	  }

	  instance.hostId = $routeParams.hostId;

	  Instance.save(instance, function(instance) {
	    $location.path('/hosts/' + $routeParams.hostId);
	  });
	};
}]);